### Deploy
Run below cli to deploy to its host
`npm run deploy` or   
`yarn deploy`

## Result
Browse to url to see the deployed application
[https://ubbn.github.io/kalendar/](https://ubbn.github.io/kalendar/)
