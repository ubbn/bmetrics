import React, {useEffect} from 'react';
import {Route, Switch} from "react-router-dom";
import {connect} from "react-redux";
import CssBaseline from '@material-ui/core/CssBaseline';
import Home from './components/Home';
import {DrawerStyle} from './styles/DrawerStyle';
import DayCounts from "./components/DayCounts";
import Autopilot from "./components/autopilot";
import Pulse from "./components/pulse/Pulse";
import Fit from "./components/fit/Fit";
import Hall from "./components/hall";
import {thunkFetchData} from "./redux/thunk";
import AppDrawer from "./components/AppDrawer";

export const BASE_URL = "/kalendar"

const AppComp = ({fresh, fetchData}) => {
  const classes = DrawerStyle()

  useEffect(() => {
    if (!fresh) {
      fetchData()
    }
  }, [fresh, fetchData])

  return (
    <div className={classes.root}>
      <CssBaseline/>
      <AppDrawer/>
      <main className={classes.content} id={"container"}>
        <div className={classes.toolbar}/>
        <DayCounts/>
        <Switch>
          <Route path={`${BASE_URL}/`} exact component={Home}/>
          <Route path={`${BASE_URL}/autopilot`} component={Autopilot}/>
          <Route path={`${BASE_URL}/pulse`} component={Pulse}/>
          <Route path={`${BASE_URL}/fit`} component={Fit}/>
          <Route path={`${BASE_URL}/hall`} component={Hall}/>
        </Switch>
      </main>
    </div>
  );
}

export default connect(
  ({loading, items, fresh}) => ({
    loading, items, fresh
  }),
  dispatch => ({
    fetchData: () => dispatch(thunkFetchData()),
  })
)(AppComp);