import { makeStyles } from '@material-ui/core/styles';

export const AreaGraphTooltipStyle =  makeStyles(theme => ({
  back: {
    backgroundColor: "white",
    padding: 5,
    borderRadius: 5,
  },
  head: {
    boxShadow: "none",
    fontWeight: "bold",
  },
  tooltip: {
    margin: 0,
    padding: 0,
  },
  good: {
    color: "green"
  },
  bad: {
    color: "red"
  }
}))