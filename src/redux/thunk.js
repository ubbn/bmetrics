import {default as axios} from 'axios';
import {FETCH_DATA, FETCH_START} from "./actions";

const API_URL = `https://spreadsheets.google.com/feeds/cells/1uooX3G2vGfKzKgwndkNZTLuqdRC7_RK-4VbwbhadpwA/1/public/full?alt=json`
const DATA_ROW = 2
const SUNDAY = 0

/**
 * Excel file shall not contain data after last days record
 *
 * @returns {function(*): Promise<void>}
 */
export const thunkFetchData = () => {
  return dispatch => {
    dispatch({type: FETCH_START})
    return axios.get(`${API_URL}`)
    .then(({data}) => {
      const entries = data.feed.entry
      const cleaned = entries.map(entry => entry["gs$cell"]).filter(
        entry => +(entry.row) >= DATA_ROW)
      const days = []
      let weekNumber = 1
      let dayNumber = 1
      let currentDay = 1 // last day when minutes data was registered
      let [minutesSum, deepSum] = [0, 0]
      cleaned.forEach(item => {
        const oneDay = days[+(item.row) - DATA_ROW] || {}
        const columnId = +item.col
        if (columnId === 1) { // Date column
          oneDay.date = item["$t"]
          oneDay.week = weekNumber
          oneDay.day = dayNumber
          const date = new Date(oneDay.date)
          oneDay.weekday = date.getDay()
          if (oneDay.weekday === SUNDAY) {
            weekNumber += 1
          }
          dayNumber += 1
        } else if (item["$t"] === item["inputValue"]) {
          // when there is no formula, just raw data
          if (columnId === 2) { // minutes column
            oneDay.minutes = +item["$t"]
            minutesSum += oneDay.minutes
            oneDay.minutesSum = minutesSum
            currentDay = dayNumber
          } else if (columnId === 3) {
            oneDay.pulses = +item["$t"]
          } else if (columnId === 4) {
            oneDay.fits = +item["$t"]
          } else if (columnId === 5) {  // deep column
            oneDay.deep = +item["$t"]
            deepSum += oneDay.deep
            oneDay.deepSum = deepSum
          }
        } else if (columnId === 4 && +item["$t"] > 0) {
          // To get data in Fit column which has formula
          oneDay.fits = +item["$t"]
        }
        days[+(item.row) - DATA_ROW] = oneDay
      })
      dispatch({
        type: FETCH_DATA,
        payload: {
          days,
          total: minutesSum,
          day: currentDay
        }
      });
    }).catch(error => console.error(error));
  };
}
