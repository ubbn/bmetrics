export const FETCH_START = 'FETCH_START';
export const FETCH_DATA = 'FETCH_DATA';
export const CLEAR_CACHE = 'CLEAR_CACHE';