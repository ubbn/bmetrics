import {CLEAR_CACHE, FETCH_DATA, FETCH_START} from "./actions";
import {addIfValid} from "../utils";

const initState = {
  loading: false,
  fresh: false,
  items: [],
  day: 0,
  total: 0,
}

export default (state = initState, action) => {
  switch (action.type) {
    case FETCH_START:
      return {
        ...state,
        loading: true,
        fresh: false,
      }
    case FETCH_DATA:
      const {days, day, total} = action.payload
      const minuteCounts = []
      const deepCounts = []
      const weekly = days.reduce((acc, curr) => {
        const {week, minutes, pulses, deep} = curr
        const oneWeek = acc[week - 1] || {week}
        const countMinute = minuteCounts[week - 1] || {week: 0}
        const countDeep = deepCounts[week - 1] || {week: 0}
        addIfValid(oneWeek, "minutes", minutes)
        addIfValid(oneWeek, "pulses", pulses)
        addIfValid(oneWeek, "deep", deep)
        if (typeof minutes == 'number') {
          countMinute.week += 1
          oneWeek.avgMin = +(oneWeek.minutes / countMinute.week).toFixed(0)
        }
        if (typeof deep == 'number') {
          countDeep.week += 1
          oneWeek.avgDeep = +(oneWeek.deep / countDeep.week).toFixed(0)
        }
        acc[week - 1] = oneWeek
        minuteCounts[week - 1] = countMinute
        deepCounts[week - 1] = countDeep
        return acc;
      }, [])
      return {
        ...state,
        items: days,
        weekly,
        loading: false,
        fresh: true,
        day,
        total,
      }
    case CLEAR_CACHE:
      return {
        ...state,
        fresh: false,
      }
    default:
      return state
  }
}

