import React from "react"
import { connect } from "react-redux"
import Daily from "./Daily"
import VerticalTabs from "./VerticalTabs"
import Weekly from "./Weekly"

const Hall = ({ daily, weekly }) => {
  console.log(daily)

  const tabs = [
    {
      name: "Daily",
      comp: <Daily data={daily} />,
    },
    {
      name: "Weekly",
      comp: <Weekly data={weekly} />,
    },
  ]
  return (
    <>
      <VerticalTabs tabs={tabs} />
    </>
  )
}

const days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]

const giveMedal = (place) => {
  switch (place) {
    case 0:
      return "🥇"
    case 1:
      return "🥈"
    case 2:
      return "🥉"
    default:
      return undefined
  }
}

const fillWithTop3 = (arr = []) => {
  const sortedUniq = new Set(arr.map((i) => i.score).sort((a, b) => b - a))
  const top3 = [...sortedUniq].slice(0, 3)
  return arr.map((i) => ({
    ...i,
    top: giveMedal(top3.indexOf(i.score)),
  }))
}

export default connect(({ items }) => {
  const daily = items
    .filter(({ minutes }) => typeof minutes == "number")
    .map(({ day, date, weekday, week, deep, minutes, pulses = 0 }) => ({
      day,
      date,
      deep,
      minutes,
      pulses,
      week,
      weekday: days[weekday],
      score: (deep || 0) - minutes,
    }))
  const weekly = daily.reduce((acc, curr) => {
    const week = acc[curr.week] || { week: curr.week }
    week[curr.weekday] = curr.score
    let weeklyScore = week.score || 0
    week.score = weeklyScore + curr.score
    acc[curr.week] = week
    return acc
  }, [])
  return {
    daily: fillWithTop3(daily),
    weekly: fillWithTop3(weekly),
  }
})(Hall)
