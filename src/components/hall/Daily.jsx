import React from "react"
import Table from "./Table"

const columns = [
  {
    Header: "Daily",
    columns: [
      {
        Header: "Day",
        accessor: "day",
      },
      {
        Header: "Date",
        accessor: "date",
      },
      {
        Header: "Day",
        accessor: "weekday",
      },
      {
        Header: "Week",
        accessor: "week",
      },
    ],
  },
  {
    Header: "Metrics",
    columns: [
      {
        Header: "Deep",
        accessor: "deep",
      },
      {
        Header: "Min",
        accessor: "minutes",
      },
      {
        Header: "Pulse",
        accessor: "pulses",
      },
    ],
  },
  {
    Header: "Summary",
    columns: [
      {
        Header: "Score",
        accessor: "score",
        sortType: "basic",
      },
      {
        Header: "Medal",
        accessor: "top",
      },
    ],
  },
]

export default ({ data }) => {
  return <Table columns={columns} data={data} />
}
