import React from "react"
import Table from "./Table"

const columns = [
  {
    Header: "Weeks",
    accessor: "week",
  },
  {
    Header: "Week days",
    columns: [
      {
        Header: "Mon",
        accessor: "Mon",
        sortType: "basic",
      },
      {
        Header: "Tue",
        accessor: "Tue",
        sortType: "basic",
      },
      {
        Header: "Wed",
        accessor: "Wed",
        sortType: "basic",
      },
      {
        Header: "Thu",
        accessor: "Thu",
        sortType: "basic",
      },
      {
        Header: "Fri",
        accessor: "Fri",
        sortType: "basic",
      },
      {
        Header: "Sat",
        accessor: "Sat",
        sortType: "basic",
      },
      {
        Header: "Sun",
        accessor: "Sun",
        sortType: "basic",
      },
    ],
  },
  {
    Header: "Summary",
    columns: [
      {
        Header: "Score",
        accessor: "score",
        sortType: "basic",
      },
      {
        Header: "Medal",
        accessor: "top",
      },
    ],
  },
]

export default ({ data }) => {
  return <Table columns={columns} data={data} />
}
