import React from 'react';
import CalendarHeatmap from 'react-calendar-heatmap';
import ReactTooltip from 'react-tooltip';
import '../../styles.css';

const printTips = (value, suffix) => {
  if (!value.count) {
    return {'data-tip': `${value.date}`}
  }
  return {
    'data-tip': `${value.date}: ${value.count} ${suffix}`,
  };
}

export default ({data, mapper, title, tipText, ...props}) => {
  return <>
    <p>{title}</p>
    <CalendarHeatmap
      {...props}
      startDate={"2019-12-31"}
      endDate={"2020-12-31"}
      tooltipDataAttrs={v => printTips(v, tipText)}
      showWeekdayLabels={true}
      onClick={value => alert(`Clicked on value with count: ${value.count}`)}
      values={data.map(mapper).map(
        v => ({...v, count: v.count === 0 ? "0" : v.count}))}
    />
    <ReactTooltip/>
  </>
}