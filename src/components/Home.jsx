import React from 'react';
import {CircularProgress, Container} from '@material-ui/core';
import AutopilotMap from "./autopilot/AutopilotMap";
import Pulse from "./pulse/Pulse";
import Fit from "./fit/Fit";
import {connect} from "react-redux";

const HomeComp = ({loading, items}) => {
  return <Container maxWidth="md">
    {loading ? <CircularProgress/> :
      <>
        <AutopilotMap data={items}/>
        <Pulse/>
        <Fit/>
      </>
    }
  </Container>
}

export default connect(
  ({loading, items}) => ({loading, items}),
)(HomeComp);
