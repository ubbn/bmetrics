import React, {useState} from 'react'
import {useHistory} from "react-router-dom"
import {useDispatch} from "react-redux"
import clsx from 'clsx'
import {useTheme} from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import List from '@material-ui/core/List'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import InboxIcon from '@material-ui/icons/MoveToInbox'
import MailIcon from '@material-ui/icons/Mail'
import CalendarPicker from './CalendarPicker'
import {DrawerStyle} from '../styles/DrawerStyle'
import {CLEAR_CACHE} from "../redux/actions"
import {BASE_URL} from "../App"

export default () => {
  const classes = DrawerStyle()
  const theme = useTheme()
  const history = useHistory()
  const dispatch = useDispatch()
  const [open, setOpen] = useState(false)

  const handleDrawerOpen = () => setOpen(true)
  const handleDrawerClose = () => setOpen(false)
  const pushLink = path => history.push(`${BASE_URL}/${path.toLowerCase()}`)

  return <>
    <AppBar
      position="fixed"
      className={clsx(classes.appBar, {
        [classes.appBarShift]: open,
      })}
    >
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={handleDrawerOpen}
          edge="start"
          className={clsx(classes.menuButton, {
            [classes.hide]: open,
          })}
        >
          <MenuIcon/>
        </IconButton>
        <Typography variant="h6" noWrap onClick={() => {
          dispatch({type: CLEAR_CACHE})
          pushLink("")
        }}>
          Bmetrics dashboard
        </Typography>
        <CalendarPicker/>
      </Toolbar>
    </AppBar>
    <Drawer
      variant="permanent"
      className={clsx(classes.drawer, {
        [classes.drawerOpen]: open,
        [classes.drawerClose]: !open,
      })}
      classes={{
        paper: clsx({
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        }),
      }}
    >
      <div className={classes.toolbar}>
        <IconButton onClick={handleDrawerClose}>
          {theme.direction === 'rtl' ? <ChevronRightIcon/> :
            <ChevronLeftIcon/>}
        </IconButton>
      </div>
      <Divider/>
      <List>
        {['Autopilot', 'Pulse', 'Fit', 'Hall'].map((text, index) => (
          <ListItem button key={text} onClick={() => pushLink(text)}>
            <ListItemIcon>{index % 2 === 0 ? <InboxIcon/> :
              <MailIcon/>}</ListItemIcon>
            <ListItemText primary={text}/>
          </ListItem>
        ))}
      </List>
    </Drawer>
  </>
}
