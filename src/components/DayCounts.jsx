import React from 'react'
import 'date-fns'
import Chip from '@material-ui/core/Chip'
import Avatar from '@material-ui/core/Avatar'
import { getDaysInMonth } from '../utils'

const today = new Date();
export default () => {
  
  const days = getDaysInMonth(today.getMonth()) + today.getDate()
  const total = getDaysInMonth(12)
  const remaining = total - days
  return <div>
    <Chip
      avatar={<Avatar>+</Avatar>}
      variant="outlined"
      size="small"
      label={remaining}
      clickable
      color="primary"
    />
    <Chip
      avatar={<Avatar>-</Avatar>}
      variant="outlined"
      size="small"
      label={days}
      clickable
      color="secondary"
    />
    <Chip
      avatar={<Avatar>%</Avatar>}
      variant="outlined"
      size="small"
      label={`${(days/total * 100).toFixed(1)}`}
      clickable
    />
  </div>
}