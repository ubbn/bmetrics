import React from 'react';
import 'react-calendar-heatmap/dist/styles.css';
import Heatmap from '../common/Heatmap';
import {pulsesStyle} from '../../utils'
import {connect} from "react-redux";
import {thunkFetchData} from "../../redux/thunk";

const Pulse = ({items}) => {
  return (
    <Heatmap data={items} classForValue={pulsesStyle} title={'Delima'}
             mapper={v => ({date: v.date, count: v.pulses})} tipText={'times'}
    />
  );
}

export default connect(
  ({loading, items, fresh}) => ({
    loading, items, fresh
  }),
  dispatch => ({
    fetchData: () => dispatch(thunkFetchData()),
  })
)(Pulse);