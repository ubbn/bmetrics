import React from 'react'
import {
  Area,
  AreaChart,
  CartesianGrid,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis
} from "recharts"
import {minutesToSeconds, readHoursFromMinutes} from "../../utils"
import {AreaGraphTooltipStyle} from "../../styles/AreaGraphTooltipStyle";

const CustomizedAxisTick = props => {
  const {x, y, stroke, value, max, index, good} = props
  if (index === max - 2) {
    return <svg>
      <circle cx={x} cy={y} r={4} fill={good ? "green" : "red"}
              stroke="white" strokeWidth={2}/>
      {!good &&
      <text x={x + 10} y={y - 14} fill={stroke} fontSize={11}
            fontWeight={"bold"} textAnchor="left">Daily on average
      </text>
      }
      <text x={x + 10} y={y - 2}
            fill={stroke} fontSize={11}
            textAnchor="left">{minutesToSeconds(value)}
      </text>
      {!good &&
      <text x={x + 10} y={y + 10} fill={stroke} fontSize={11}
            textAnchor="left">{readHoursFromMinutes(value)}
      </text>
      }
    </svg>
  }
  return null
}

const CustomTooltip = props => {
  const classes = AreaGraphTooltipStyle()
  const {active, payload, label} = props

  if (active && payload && payload.length > 0) {
    const {date, avg, minutes} = payload[0].payload
    return (
      <div className={classes.back}>
        <p className={`${classes.head} ${classes.tooltip}`}>{date} ({label})</p>
        <p className={classes.tooltip}>{minutesToSeconds(avg)}</p>
        <p className={classes.tooltip}>{`${minutes} min, on this day`}</p>
      </div>
    )
  }
  return null
}

export default ({data = [], xKey, yKey, day, name}) => {
  return <>
    <ResponsiveContainer width='100%' height={200}>
      <AreaChart width={600} height={400} data={data} syncId="anyId"
                 margin={{top: 0, right: 10, left: 0, bottom: 0}}>
        <CartesianGrid strokeDasharray="3 3"/>
        <XAxis dataKey={xKey}/>
        <YAxis/>
        <Tooltip content={<CustomTooltip/>}/>
        <Area type='monotone' dataKey={yKey}
              label={<CustomizedAxisTick max={day} good={false}/>}
              stroke='#8884d8' fill='#8884d8' name={name}/>
        <Area type='monotone' dataKey={'avgDeep'}
              label={<CustomizedAxisTick max={day} good={true}/>}
              stroke='green' fill='green' name={name}/>
      </AreaChart>
    </ResponsiveContainer>
  </>
}