import React from 'react';
import Heatmap from '../common/Heatmap';
import {autopilotStyle} from '../../utils'

export default ({data}) => {
  return <Heatmap
    classForValue={autopilotStyle}
    data={data}
    mapper={v => ({date: v.date, count: v.minutes, week: v.week})}
    tipText={'minutes'}
  />
}