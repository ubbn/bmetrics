import React from "react"
import {
  CartesianGrid,
  Legend,
  Line,
  LineChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts"
import { AreaGraphTooltipStyle } from "../../styles/AreaGraphTooltipStyle"

const CustomizedAxisTick = ({ x, y, stroke, value }) => (
  <text x={x} y={y} dy={-4} fill={stroke} fontSize={11} textAnchor="middle">
    {value}
  </text>
)

const CustomTooltip = (props) => {
  const { back, head, tooltip, good, bad } = AreaGraphTooltipStyle()
  const { active, payload, label } = props

  if (active && payload && payload.length > 0) {
    const { avgMin, avgDeep } = payload[0].payload
    return (
      <div className={back}>
        <p className={`${head} ${tooltip}`}>Daily in week {label}</p>
        <p className={`${good} ${tooltip}`}>{avgDeep || 0} minutes</p>
        <p className={`${bad} ${tooltip}`}>{avgMin} minutes</p>
      </div>
    )
  }
  return null
}

export default ({ data = [], xKey, yKey, color = "#8884d8", name }) => {
  return (
    <>
      <ResponsiveContainer width="100%" height={200}>
        <LineChart
          width={400}
          height={200}
          data={data}
          margin={{ top: 10, right: 10, left: 5, bottom: 5 }}
        >
          <XAxis dataKey={xKey} />
          <YAxis />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip content={<CustomTooltip />} />
          <Legend />
          <Line
            dataKey={yKey}
            name={name}
            stroke={color}
            activeDot={{ r: 4 }}
            label={<CustomizedAxisTick />}
          />
          <Line
            dataKey={"avgDeep"}
            name={"Deep work daily"}
            stroke={"green"}
            activeDot={{ r: 4 }}
            label={<CustomizedAxisTick />}
          />
        </LineChart>
      </ResponsiveContainer>
    </>
  )
}
