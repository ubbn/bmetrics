import React from 'react'
import {
  Area,
  AreaChart,
  CartesianGrid,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis
} from "recharts"
import {
  readDaysFromMinutes,
  readHoursFromMinutes,
  readMonthsFromMinutes
} from "../../utils"
import {AreaGraphTooltipStyle} from "../../styles/AreaGraphTooltipStyle";

const CustomizedAxisTick = (props) => {
  const {x, y, stroke, value, max, index, good} = props
  if (index === max - 2) {
    const offset = good ? -22 : 0
    return <svg>
      <circle cx={x} cy={y} r={4} fill={good ? "green" : "red"} stroke="white"
              strokeWidth={2}/>
      <text x={x + 10} y={y - 2 + offset}
            fill={stroke} fontSize={11}
            textAnchor="left">{value} minutes
      </text>
      <text x={x + 10} y={y + 10 + offset}
            fill={stroke} fontSize={11}
            textAnchor="left">{readHoursFromMinutes(value)}
      </text>
      <text x={x + 10} y={y + 22 + offset}
            fill={stroke} fontSize={11}
            textAnchor="left">{readDaysFromMinutes(value)} days
      </text>
    </svg>
  }
  return null
}

const CustomTooltip = props => {
  const classes = AreaGraphTooltipStyle()
  const {active, payload} = props

  if (active && payload && payload.length > 0) {
    const {minutesSum} = payload[0].payload
    return (
      <div className={classes.back}>
        <p className={classes.tooltip}>{`${minutesSum} minutes`}</p>
        <p className={classes.tooltip}>
          {`${readHoursFromMinutes(minutesSum)}`}
        </p>
        <p className={classes.tooltip}>
          {`${readDaysFromMinutes(minutesSum)} days`}
        </p>
        <p className={classes.tooltip}>
          {`${readMonthsFromMinutes(minutesSum)}`}
        </p>
      </div>
    )
  }
  return null
}

export default ({data = [], xKey, yKey, name, day}) => {
  return <>
    <ResponsiveContainer width='100%' height={160}>
      <AreaChart width={600} height={300} data={data} syncId="anyId"
                 margin={{top: 9, right: 10, left: 0, bottom: 2}}>
        <CartesianGrid strokeDasharray="3 3"/>
        <XAxis dataKey={xKey} hide={true}/>
        <YAxis/>
        <Tooltip content={<CustomTooltip/>}/>
        <Area type='monotone' dataKey={yKey} stroke='#cf513d' fill='#ec9488'
              name={name} label={<CustomizedAxisTick max={day} good={false}/>}/>
        <Area type='monotone' dataKey={'deepSum'} stroke='green' fill='green'
              name={name} label={<CustomizedAxisTick max={day} good={true}/>}/>
      </AreaChart>
    </ResponsiveContainer>
  </>
}