import React from "react"
import { connect } from "react-redux"
import { Container } from "@material-ui/core"
import { thunkFetchData } from "../../redux/thunk"
import AverageLine from "./AverageLine"
import AutopilotMap from "./AutopilotMap"
import AverageArea from "./AverageArea"
import TotalMinutes from "./TotalMinutes"

const Autopilot = ({ items, weekly, dailyAvg, day }) => {
  return (
    <Container maxWidth="lg">
      <AutopilotMap data={items} />
      <TotalMinutes
        data={items}
        xKey="day"
        yKey="minutesSum"
        name="Total sum"
        color="#82ca9d"
        day={day}
      />
      <AverageArea
        data={dailyAvg}
        xKey="day"
        yKey="avg"
        name="Daily average in a year"
        day={day}
      />
      <AverageLine
        data={weekly}
        xKey="week"
        yKey="avgMin"
        name="Daily average in a week"
        color="red"
      />
    </Container>
  )
}

export default connect(
  ({ items, weekly = [], day }) => ({
    items,
    weekly,
    dailyAvg: items.map(({ day, minutesSum, date, minutes, deepSum }) => ({
      day,
      date,
      minutes,
      avg: minutesSum != null ? +(minutesSum / day).toFixed(2) : null,
      avgDeep: deepSum != null ? +(deepSum / day).toFixed(2) : null,
    })),
    day,
  }),
  (dispatch) => ({
    fetchData: () => dispatch(thunkFetchData()),
  })
)(Autopilot)
