import React from 'react';
import 'react-calendar-heatmap/dist/styles.css';
import Heatmap from '../common/Heatmap';
import {fitnessStyle} from '../../utils'
import {connect} from "react-redux";
import {thunkFetchData} from "../../redux/thunk";

const Fit = ({items}) => {
  return (
    <Heatmap data={items} classForValue={fitnessStyle} title={'Move fit'}
             mapper={v => ({date: v.date, count: v.fits})} tipText={'times'}
    />
  );
}

export default connect(
  ({loading, items, fresh}) => ({
    loading, items, fresh
  }),
  dispatch => ({
    fetchData: () => dispatch(thunkFetchData()),
  })
)(Fit);
