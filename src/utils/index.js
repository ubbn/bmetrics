export const autopilotStyle = value => {
  if (value && value.count) {
    if (+value.count === 0) {
      return `color-green-3`;
    } else if (+value.count <= 30) {
      return `color-green-1`;
    } else {
      return `color-red-${Math.ceil(value.count / 60)}`;
    }
  }
  return 'color-empty';
}

export const pulsesStyle = value => {
  if (value && value.count) {
    if (+value.count <= 1) {
      return `color-red-1`;
    }
    return `color-green-${Math.min(Math.floor(value.count / 2), 7)}`;
  }
  return 'color-empty';
}

export const fitnessStyle = value => {
  if (value && value.count) {
    if (+value.count === 0) {
      return `color-red-1`;
    }
    return `color-green-3`;
  }
  return 'color-empty';
}

export const getDaysInMonth = month => {
  if (month < 1 || month > 12) {
    return 0;
  }
  const daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
  const today = new Date();
  let days = 0
  for (let index = 0; index < month; index++) {
    days += daysInMonth[index]
  }
  if (today.getFullYear() % 4 === 0) {
    return days + 1
  } else {
    return days
  }
}

export const readHoursFromMinutes = minutes => {
  const remainder = minutes % 60
  const hours = (minutes - remainder) / 60
  if (hours > 0) {
    if (remainder > 0) {
      return `${hours} hours ${remainder.toFixed(0)} minutes`
    } else {
      return `${hours} hours`
    }
  } else {
    return `${minutes} minutes`
  }
}

export const readDaysFromMinutes = minutes => {
  const remainder = minutes % 60
  const hours = (minutes - remainder) / 60 + (remainder > 0 ? 1 : 0)
  const daysRem = hours % 8
  return ((hours - daysRem) / 8) + (daysRem > 0 ? 1 : 0)
}

export const readMonthsFromMinutes = minutes => {
  const days = readDaysFromMinutes(minutes)
  const remainder = days % 22
  const months = (days - remainder) / 22
  return `${months} months ${remainder} days`
}

export const minutesToSeconds = minutes => {
  const whole = Math.floor(minutes)
  const seconds = ((minutes - whole) * 60).toFixed(0)
  return `${whole} min ${seconds} sec`
}

export const addIfValid = (obj, key, value2) => {
  if (typeof value2 == 'number') {
    if (typeof obj[key] == 'number') {
      obj[key] += value2
    } else {
      obj[key] = value2
    }
  }
}